package com.jcq;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.jcq.boxBarScan.DataUtils;
import com.jcq.boxBarScan.ExcelUtil;
import com.jcq.boxBarScan.NumberField;
import com.jcq.boxBarScan.SwingUtils;
import com.jcq.boxBarScan.sqlite.SQLiteCRUD;
import com.jcq.boxBarScan.sqlite.SQLiteConn;
import com.jcq.voice.utils.SocketClientThreadForVoice;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
/**
 * @ClassName: MainFrame
 * @Description:主页面.
 * @author jcq
 * @date 2015-3-24 上午08:38:48
 */
public class MainFrame extends JFrame {
	private static final int WIDTH = 730;
	private static final int HEIGHT = 500;
	private static NumberField submitNumNF;
	private static JTextField boxCodeTF;// 条码输入框.
	private static JLabel boxLabel; // 显示周转箱条码.
	private static JLabel boxCntValueLabel; // 显示已经扫描周转箱条码数量.
	private static JLabel measureCntValueLabel; // 显示已经扫描计量器具数量
	private static int totalMeasureCnt;// 已经扫描计量器具总数量.
	private static JButton enterBtn;// 条码确认按钮.
	private static JButton clearBtn;// 清空文本框内容.
	private static JLabel countLabel;// 计量器具框.
	private DefaultTableModel tableModel; // 表格模型对象.
	private JTable table;// 计量器具列表.
	private int count = 0;// 扫描计数.
	private List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();// 保存数据.
	private StringBuffer boxBarCodeBuf = new StringBuffer();// 周转箱条形码.
	private String tableName = "JCQ_BAR_CODE";
	private SQLiteCRUD sqliteCRUD;

	public MainFrame() {
		setTitle("条码扫描工具jcq V15.7.1");
		setSize(WIDTH, HEIGHT);
		initView();
		initEvent();
		boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
		//boxCodeTF.requestFocusInWindow();
		//每次开启工具生成一个数据库.
		try {
			sqliteCRUD = new SQLiteCRUD(new SQLiteConn().getConnection());
			sqliteCRUD.createTable("drop table if exists "+tableName+";create table "+tableName+" (BOX_BAR_CODE, MEASURE_BAR_CODE);");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void initView() {
		Box globalBox = Box.createVerticalBox();
		getContentPane().add(globalBox, BorderLayout.NORTH);
		globalBox.setAutoscrolls(true);

		// 头部全部变量
		JPanel headPanel = new JPanel();
		headPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		globalBox.add(headPanel);
		headPanel.setAutoscrolls(true);
		headPanel.setAlignmentY(20);
		headPanel.add(Box.createHorizontalStrut(20));
		JLabel submitLabel = new JLabel("填写计量器具自动提交的数量:");
		headPanel.add(submitLabel);
		headPanel.add(Box.createHorizontalStrut(5));
		submitNumNF = new NumberField(16);
		headPanel.add(submitNumNF);
		submitNumNF.setToolTipText("数量只能为数字.");
		submitNumNF.setText("12");
		// 组装数量显示行.
		JPanel showBoxCntPanel = new JPanel();
		showBoxCntPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		globalBox.add(showBoxCntPanel);
		showBoxCntPanel.add(Box.createHorizontalStrut(20));
		JLabel boxCntLabel = new JLabel("已周转箱数量:");
		showBoxCntPanel.add(boxCntLabel);
		headPanel.add(Box.createHorizontalStrut(5));
		// 显示已周转箱数量
		boxCntValueLabel = new JLabel("0 (箱)");
		showBoxCntPanel.add(boxCntValueLabel);

		JLabel measueTipLabel = new JLabel("已扫描计量器具数量:");
		showBoxCntPanel.add(measueTipLabel);
		headPanel.add(Box.createHorizontalStrut(5));
		// 显示已扫描计量器具数量
		measureCntValueLabel = new JLabel("0 (只)");
		showBoxCntPanel.add(measureCntValueLabel);

		// 组装扫描输入框.
		JPanel boxBarpanel = new JPanel();
		boxBarpanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		globalBox.add(boxBarpanel);
		boxBarpanel.add(Box.createHorizontalStrut(20));
		boxBarpanel.setAutoscrolls(true);
		boxBarpanel.setAlignmentY(20);

		boxCodeTF = new JTextField(32);
		SwingUtils.enterPressesWhenFocused(boxCodeTF, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				whenBarCodeEnter();
			}
		});
		boxCodeTF.setToolTipText("条码扫描框.");
		boxCodeTF.setMaximumSize(boxCodeTF.getPreferredSize());
		boxBarpanel.add(boxCodeTF);
		boxBarpanel.add(Box.createHorizontalStrut(20));
		enterBtn = new JButton("提交文本框内容");
		boxBarpanel.add(enterBtn);
		boxBarpanel.add(Box.createHorizontalStrut(20));
		clearBtn = new JButton("清空文本框内容");
		boxBarpanel.add(clearBtn);
		// 显示周转箱条形码.
		JPanel boxBarCodepanel = new JPanel();
		boxBarCodepanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		globalBox.add(boxBarCodepanel);
		boxBarCodepanel.add(Box.createHorizontalStrut(20));
		JLabel borCodeLabel = new JLabel("周转箱条形码:");
		boxBarCodepanel.add(borCodeLabel);
		boxLabel = new JLabel();
		boxBarCodepanel.add(boxLabel);

		JPanel measureCntpanel = new JPanel();
		measureCntpanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		measureCntpanel.add(Box.createHorizontalStrut(20));
		JLabel countTitleLabel = new JLabel();
		measureCntpanel.add(countTitleLabel);
		countTitleLabel.setText("计量器具数量:");
		countLabel = new JLabel();
		measureCntpanel.add(countLabel);
		globalBox.add(measureCntpanel);

		String[] columnNames = { "计量器具条形码" }; // 列名
		String[][] tableVales = {}; // 数据
		tableModel = new DefaultTableModel(tableVales, columnNames);
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table); // 支持滚动
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		// jdk1.6
		// 排序:
		table.setRowSorter(new TableRowSorter(tableModel));
		initTableOptPage(globalBox);
		
		// 导出全部代码
		JPanel exportpanel = new JPanel();
		JButton exportButton = new JButton("导出excel");
		exportpanel.add(exportButton);
		exportButton.addActionListener(new ActionListener() {// 添加事件
					public void actionPerformed(ActionEvent e) {
						try {
							if (mapList != null && mapList.size() > 0) {
								String boxCnt = boxCntValueLabel.getText();
								String measureCnt = measureCntValueLabel.getText();
								String fileName = "周转箱("+ boxCnt+ ")计量器具("+ measureCnt+")"+ DataUtils.date2Str(new Date(),DataUtils.date_sdf_wz);
								ExcelUtil.writeExcel(mapList, fileName);
							} else {
								JOptionPane.showMessageDialog(null,"您可以导出的数据为空！");
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
					}
				});
		JButton exportForDbButton = new JButton("从数据库导出excel");
		exportpanel.add(exportForDbButton);
		exportForDbButton.addActionListener(new ActionListener() {// 添加事件
					public void actionPerformed(ActionEvent e) {
						try {
							List<Map<String, String>> dbMapList = new ArrayList<Map<String, String>>();
							String sql ="SELECT distinct(box_bar_code)  FROM "+tableName;
							List<String> boxBarCodeList = sqliteCRUD.selectOneBySql(sql);
							if(boxBarCodeList!=null&&boxBarCodeList.size()>0){
								List<String> measureBarCodeList = null;
								for(String boxBarCode:boxBarCodeList){
									Map<String, String> map = new HashMap<String, String>();
									dbMapList.add(map);
									StringBuffer measureBarCodeBuf = new StringBuffer();
									measureBarCodeList = sqliteCRUD.selectOne(tableName, "MEASURE_BAR_CODE","BOX_BAR_CODE", boxBarCode);
									if(measureBarCodeList!=null&&measureBarCodeList.size()>0){
										for(String measureBarCode:measureBarCodeList){
											measureBarCodeBuf.append(measureBarCode+",");
										}
									}
									map.put(boxBarCode, measureBarCodeBuf.toString().length() > 0 ? measureBarCodeBuf.toString().substring(0, measureBarCodeBuf.toString().length() - 1) : "");
								}
								if (dbMapList != null && dbMapList.size() > 0) {
									int measureCnt = sqliteCRUD.getTableCount(tableName);
									String fileName = "周转箱("+ boxBarCodeList.size()+ ")计量器具("+ measureCnt+")"+DataUtils.date2Str(new Date(),DataUtils.date_sdf_wz);
									ExcelUtil.writeExcel(dbMapList, fileName);
								} else {
									JOptionPane.showMessageDialog(null,"您可以导出的数据为空！");
								}
							} else {
								JOptionPane.showMessageDialog(null, "您可以导出的数据为空！");
							}
							
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
					}
				});
		
		
		JButton exportForDbButtonCol = new JButton("从数据库导出excel(横向)");
		exportpanel.add(exportForDbButtonCol);
		exportForDbButtonCol.addActionListener(new ActionListener() {// 添加事件
					public void actionPerformed(ActionEvent e) {
						try {
							List<Map<String, String>> dbMapList = new ArrayList<Map<String, String>>();
							String sql ="SELECT distinct(box_bar_code)  FROM "+tableName;
							List<String> boxBarCodeList = sqliteCRUD.selectOneBySql(sql);
							if(boxBarCodeList!=null&&boxBarCodeList.size()>0){
								List<String> measureBarCodeList = null;
								for(String boxBarCode:boxBarCodeList){
									Map<String, String> map = new HashMap<String, String>();
									dbMapList.add(map);
									StringBuffer measureBarCodeBuf = new StringBuffer();
									measureBarCodeList = sqliteCRUD.selectOne(tableName, "MEASURE_BAR_CODE","BOX_BAR_CODE", boxBarCode);
									if(measureBarCodeList!=null&&measureBarCodeList.size()>0){
										for(String measureBarCode:measureBarCodeList){
											measureBarCodeBuf.append(measureBarCode+",");
										}
									}
									map.put(boxBarCode, measureBarCodeBuf.toString().length() > 0 ? measureBarCodeBuf.toString().substring(0, measureBarCodeBuf.toString().length() - 1) : "");
								}
								if (dbMapList != null && dbMapList.size() > 0) {
									int measureCnt = sqliteCRUD.getTableCount(tableName);
									String fileName = "周转箱("+ boxBarCodeList.size()+ ")计量器具("+ measureCnt+")"+DataUtils.date2Str(new Date(),DataUtils.date_sdf_wz);
									ExcelUtil.writeExcelRow(dbMapList, fileName);
								} else {
									JOptionPane.showMessageDialog(null,"您可以导出的数据为空！");
								}
							} else {
								JOptionPane.showMessageDialog(null, "您可以导出的数据为空！");
							}
							
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
					}
				});
		
		
		
		JButton clearAllButton = new JButton("清空全部数据");
		exportpanel.add(clearAllButton);
		clearAllButton.addActionListener(new ActionListener() {// 添加事件
					public void actionPerformed(ActionEvent e) {
						Object[] options ={"确认清空","取消"};
						int response =JOptionPane.showOptionDialog(null,"是否确认清空全部数据", "警告", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE,null,options, options[0]);
						if(response==0){
							clearAll();
						}
						boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
					}
				});
		getContentPane().add(exportpanel, BorderLayout.SOUTH);
	}

	// 组装表编辑页面
	public void initTableOptPage(Box globalBox) {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(Box.createHorizontalStrut(20));
		globalBox.add(panel);
		final JButton delButton = new JButton("删除");
		panel.add(delButton);
		delButton.addActionListener(new ActionListener() {// 添加事件
					public void actionPerformed(ActionEvent e) {
						int selectedRow = table.getSelectedRow();// 获得选中行的索引
						if (selectedRow != -1) // 存在选中行
						{
							tableModel.removeRow(selectedRow); // 删除行
							count--;
							if (count <= 0) {
								count = 0;
							}
							countLabel.setText(count - 1 + "");
						}
						boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
						boxCodeTF.requestFocusInWindow();
					}
				});
		final JButton submitButton = new JButton("提交本次扫描结果");
		panel.add(submitButton);
		submitButton.addActionListener(new ActionListener() {// 添加事件
					public void actionPerformed(ActionEvent e) {
						submitBarCode();
						boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
					}
				});
		panel.add(Box.createHorizontalStrut(20));
		JButton resetBtn = new JButton("重置本次扫描结果");
		panel.add(resetBtn);
		resetBtn.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				clearPageData();
				boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
			}
		});
		panel.add(Box.createHorizontalStrut(20));
		JLabel tipLabel = new JLabel("临时文件保存在："+ExcelUtil.tempFilePath);
		panel.add(tipLabel);
	}

	public void initEvent() {
		enterBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				whenBarCodeEnter();
				boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
			}
		});

		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boxCodeTF.setText("");
				boxCodeTF.dispatchEvent(new FocusEvent(boxCodeTF, FocusEvent.FOCUS_GAINED));
			}
		});
	}

	// 条形码文本框回车填充数据.
	public void whenBarCodeEnter() {
		String barCode = boxCodeTF.getText();
		if (barCode != null && barCode.length() > 0) {
			if (count == 0) {
				boxLabel.setText(barCode);
				String boxBarCodeStr = boxBarCodeBuf.toString();
				System.out.println(boxBarCodeStr);
				if (isBoxScan(barCode)||isInScanData(barCode)) {
					boxLabel.setText("");
					boxCodeTF.setText("");
//					JOptionPane.showMessageDialog(null, "周转箱【" + barCode	+ "】已经存在!");
					new SocketClientThreadForVoice("boxHadScan.wav").start();
				} else {
					new SocketClientThreadForVoice("boxHadScanNextMeasure.wav").start();
					boxCodeTF.setText("");
					count++;
				}
			} else {
				if(!boxLabel.getText().equals(barCode)){
					table.clearSelection(); // 取消选择
					boxCodeTF.setText("");
					if (!isTableHadData(barCode)) {
						addMeasureBarCode(count, barCode);
						countLabel.setText(count + "");
						count++;
					}else{
						new SocketClientThreadForVoice("measureHadScan.wav").start();
					}
					String numStr = submitNumNF.getText();
					if (numStr != null && numStr.length() > 0) {
						int submitNum = Integer.valueOf(numStr);
						if (count >= (submitNum + 1)) {
							// 提交当次数据
							submitBarCode();
						}
					}
				}
				else{
					new SocketClientThreadForVoice("canNotScanSameBox.wav").start();
//					JOptionPane.showMessageDialog(null, "周转箱【" + barCode	+ "】已经存在!");
					boxCodeTF.setText("");
				}
			}
		}
	}

	// 查询周转箱条形码是否已经扫描过.
	public boolean isBoxScan(String boxBarCode) {
		boolean flag = false;
		String[] boxBarCodeArray = boxBarCodeBuf.toString().split(",");
		for (String barCode : boxBarCodeArray) {
			if (barCode.equals(boxBarCode)) {
				flag = true;
				return true;
			}
		}
		return flag;
	}

	// 遍历表格数据返回是否包含对应的数据.
	public boolean isTableHadData(String measureBarCode) {
		boolean flag = false;
		TableModel model = table.getModel();
		for (int i = 0; i < model.getRowCount(); i++) {
			for (int j = 0; j < model.getColumnCount(); j++) {
				String value = model.getValueAt(i, j).toString();
				if (value.equals(measureBarCode)) {
					flag = true;
					table.setRowSelectionInterval(i, i);
					return flag;
				}
			}
		}
		return isInScanData(measureBarCode);
	}
	// 遍历已经扫描的数据是否包含对应的数据.
	public boolean isInScanData(String measureBarCode){
		boolean flag = false;
		for (int i = 0; i < mapList.size(); i++) {
			Map<String, String> map = mapList.get(i);
			for (Entry<String, String> entry : map.entrySet()) {
				String key = entry.getKey();
				if(!key.equals(measureBarCode)){
					String measureBarCodeValue = entry.getValue();
					String measureBarCodeArray[] = measureBarCodeValue.split(",");
					for(String barCode:measureBarCodeArray){
						if(barCode.equals(measureBarCode)){
							flag = true;
							return flag;
						}
					}
				}else{
					flag = true;
					return flag;
				}
			}
		}
		return flag;
	}

	// 获取表格数据.
	public String getTableData() {
		StringBuffer measureStr = new StringBuffer();
		TableModel model = table.getModel();
		for (int i = 0; i < model.getRowCount(); i++) {
			for (int j = 0; j < model.getColumnCount(); j++) {
				String value = model.getValueAt(i, j).toString();
				measureStr.append(value + ",");
			}
		}
		return measureStr.toString().length() > 0 ? measureStr.toString()
				.substring(0, measureStr.toString().length() - 1) : "";
	}

	// 提交扫描结果.
	public void submitBarCode() {
		String barCodeText = boxLabel.getText();
		if (barCodeText != null && barCodeText.length() > 0) {
			// 周转箱条形码添加
			boxBarCodeBuf.append(barCodeText + ",");
			totalMeasureCnt += table.getModel().getRowCount();// 递增计量器具记录数.
			measureCntValueLabel.setText(totalMeasureCnt + " (只)");
			// 提交当次数据
			Map<String, String> map = new HashMap<String, String>();
			TableModel model = table.getModel();
			String[] dataStr = new String[2];
			for (int i = 0; i < model.getRowCount(); i++) {
				for (int j = 0; j < model.getColumnCount(); j++) {
					dataStr = new String[2];
					dataStr[0] = barCodeText;
					String value = model.getValueAt(i, j).toString();
					dataStr[1] = value;
					sqliteCRUD.insert(tableName, dataStr);
				}
			}
			map.put(barCodeText, getTableData());
			mapList.add(map);
			System.out.println(mapList.toString());
			clearPageData();
		} else {
			JOptionPane.showMessageDialog(null, "周转箱不能为空!");
		}
		// 更新页面周转箱数据量.
		boxCntValueLabel.setText(mapList.size() + " (箱)");
		try {
			ExcelUtil.saveSubmitData(mapList);
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new SocketClientThreadForVoice("scanNextBox.wav").start();
	}

	// 添加计量器具条形码.
	public void addMeasureBarCode(int index, String barCode) {
		String[] rowValues = { barCode };
		tableModel.addRow(rowValues); // 添加一行
	}

	// 清空页面数据.
	public void clearPageData() {
		count = 0;
		boxCodeTF.setText("");
		boxLabel.setText("");
		countLabel.setText("");
		// 清空table数据.
		((DefaultTableModel) (table.getModel())).getDataVector().clear();
		((DefaultTableModel) (table.getModel())).fireTableDataChanged();
		table.updateUI();
	}

	// 清空全部数据.
	public void clearAll() {
		clearPageData();
		count = 0;
		mapList = new ArrayList<Map<String, String>>();
		boxCntValueLabel.setText("0(箱)");
		measureCntValueLabel.setText("0(只)");
		totalMeasureCnt = 0;
		boxBarCodeBuf.delete(0, boxBarCodeBuf.length());
	}

	public static void main(String[] args) {
    	try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MainFrame mainFrame = new MainFrame();
		int windowWidth = mainFrame.getWidth();
		int windowHeight = mainFrame.getHeight();
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		int screenWidth = screenSize.width;
		int screenHeight = screenSize.height;
		mainFrame.setLocation(screenWidth / 2 - windowWidth / 2, screenHeight
				/ 2 - windowHeight / 2);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImageIcon logoIcon=new ImageIcon(ClassLoader.getSystemResource("images/logo.png"));
				  logoIcon=new ImageIcon(logoIcon.getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH));
		mainFrame.setIconImage(logoIcon.getImage());
		mainFrame.show();
	}
}
