package com.jcq.voice.player;
/**
 * @ClassName: Player
 * @Description:��������.
 * @author jcq
 * @date 2015-3-24 ����08:38:48
 */
public class Player {
	public static void playVoice(String file){
		String osName = System.getProperties().getProperty("os.name").toUpperCase();
		if(osName.contains("WINDOWS")){
			 playerForWindow.play(file);
		}else if(osName.contains("LINUX")){
			 playerForLinux.play(file);
		}
	}
	public static void main(String[] args) {
		String file = "scanNextBox.wav";
		String osName = System.getProperties().getProperty("os.name").toUpperCase();
		if(osName.contains("WINDOWS")){
			playerForWindow.play(file);
		}else if(osName.contains("LINUX")){
			playerForLinux.play(file);
		}
	}
}
