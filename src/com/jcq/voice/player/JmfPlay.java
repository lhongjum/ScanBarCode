package com.jcq.voice.player;
import java.io.File;
import java.net.URL;
import javax.media.ControllerClosedEvent;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.EndOfMediaEvent;
import javax.media.Manager;
import javax.media.Player;
import javax.media.ResourceUnavailableEvent;
/**
 * @ClassName: JmfPlay
 * @Description:���Բ���.
 * @author jcq
 * @date 2015-3-24 ����08:38:48
 */
public class JmfPlay
{
	private Player player = null;
	private Boolean end = false;
	public void play(URL url) throws Exception
	{
		end = false;
		player = Manager.createPlayer(url);
		player.addControllerListener(new ControllerListener()
		{
			@Override
			public void controllerUpdate(ControllerEvent controllerevent)
			{
				if(controllerevent instanceof EndOfMediaEvent
						|| controllerevent instanceof ResourceUnavailableEvent
						|| controllerevent instanceof ControllerClosedEvent) //
					end = true;
			}
		});
		player.start();
		for (; !end;)
		{
			Thread.sleep(10);
		}
		player.close();
		player.deallocate();
		player = null;
	}
	@SuppressWarnings("deprecation")
	public void play(File file) throws Exception
	{
		play(file.toURL());
	}
}
