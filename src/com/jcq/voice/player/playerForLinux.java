package com.jcq.voice.player;
import java.io.File;

import com.jcq.voice.utils.CommandHelper;
import com.jcq.voice.utils.CommandResult;
/**
 * @ClassName: playerForLinux
 * @Description:播放声音forLinux.
 * @author jcq
 * @date 2015-3-24 上午08:38:48
 */
public class playerForLinux {
	public static synchronized void play(String voiceFile) {
		try{
			String cmdStr = "mplayer -af volume=40 -softvol -softvol-max 200 "+voiceFile;
			//进行播放语音的操作.
			int timeout = 1000*600;
	        CommandHelper.DEFAULT_TIMEOUT = timeout;
	        CommandResult result = CommandHelper.exec(cmdStr);
	        if (result != null) {
	            System.out.println("Output:" + result.getOutput());
	            System.out.println("Error:" + result.getError());
	        }
		}catch(Exception e){
			try {
				new JmfPlay().play(new File(voiceFile));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}
}
