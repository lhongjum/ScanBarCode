package com.jcq.boxBarScan;
import java.io.BufferedReader;
 import java.io.File;
 import java.io.FileInputStream;
 import java.io.FileNotFoundException;
 import java.io.FileOutputStream;
 import java.io.FileReader;
 import java.io.IOException;
 import java.io.InputStreamReader;
import java.io.PrintWriter;

 /**
  * @author jcq
  * @time 2011-12-12 2011
  */
 public class FileUtils {
     @SuppressWarnings("static-access")
     public static void main(String[] args) {
     }

     private static String path = "";
     private static String dbFileName = path + "db.db";
     /**
      * 创建文件
      *
      * @throws IOException
      */
     public static boolean createDbFile() throws IOException {
         boolean flag = false;
         File filename = new File(dbFileName);
         if (!filename.exists()) {
             filename.createNewFile();
             flag = true;
         }
         return flag;
     }

     
     public static String readDate() {
         // 定义一个待返回的空字符串
         String strs = "";
         try {
        	 File filename = new File(dbFileName);
             if (!filename.exists()) {
                 filename.createNewFile();
             }
             FileReader read = new FileReader(new File(dbFileName));
             StringBuffer sb = new StringBuffer();
             char ch[] = new char[1024];
             int d = read.read(ch);
             while (d != -1) {
                 String str = new String(ch, 0, d);
                 sb.append(str);
                 d = read.read(ch);
             }
             strs = sb.toString();
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
         if(strs==null||strs.length()<=0){
        	 strs="C";
 		 }
 		 try {
			FileUtils.writeTxtFileAppend(strs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         return strs;
     }
     /////////////////////////////////////////////////////////////////
     /**
      * 写文件
      *
      * @param newStr
      *            新内容
      * @throws IOException
      */
     public static boolean writeTxtFileAppend(String newStr) throws IOException {
         File filename = new File(dbFileName);
         if (!filename.exists()) {
             filename.createNewFile();
         }
         // 先读取原有文件内容，然后进行写入操作
         boolean flag = false;
         String temp = "";

         FileInputStream fis = null;
         InputStreamReader isr = null;
         BufferedReader br = null;

         FileOutputStream fos = null;
         PrintWriter pw = null;
         try {
             // 文件路径
             File file = new File(dbFileName);
             // 将文件读入输入流
             fis = new FileInputStream(file);
             isr = new InputStreamReader(fis);
             br = new BufferedReader(isr);
//             StringBuffer buf = new StringBuffer();
//             buf.append(filein);
//             // 保存该文件原有的内容
//             for (int j = 1; (temp = br.readLine()) != null; j++) {
//                 buf = buf.append(temp);
//                 // System.getProperty("line.separator")
//                 // 行与行之间的分隔符 相当于“\n”
//                 buf = buf.append(System.getProperty("line.separator"));
//             }
             fos = new FileOutputStream(file);
             pw = new PrintWriter(fos);
             pw.write(newStr.toString().toCharArray());
             pw.flush();
             flag = true;
         } catch (IOException e1) {
             // TODO 自动生成 catch 块
             throw e1;
         } finally {
             if (pw != null) {
                 pw.close();
             }
             if (fos != null) {
                 fos.close();
             }
             if (br != null) {
                 br.close();
             }
             if (isr != null) {
                 isr.close();
             }
             if (fis != null) {
                 fis.close();
             }
         }
         return flag;
     }
 }