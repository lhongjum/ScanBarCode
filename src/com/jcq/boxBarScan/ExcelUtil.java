package com.jcq.boxBarScan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
/**
 * @ClassName: ExcelUtil
 * @Description:EXCEL工具.
 * @author jcq
 * @date 2015-3-24 上午08:38:48
 */
public class ExcelUtil {
	public static String fileName = "条码扫描临时文件.xls";
    public static String tempFilePath = FileUtils.readDate()+":"+File.separator+fileName;
	//断电保存数据.
	public static void saveSubmitData(List<Map<String, String>> mapList) throws FileNotFoundException, IOException, RowsExceededException, WriteException{
		//String tempFilePath = FileUtils.readDate()+":"+File.separator+fileName;
		String tempFilePath = fileName;
		File f = new File(tempFilePath);
		if (f.exists()){
			f.delete();
		}else{
			f.createNewFile();
		}
		WritableWorkbook wwb = Workbook	.createWorkbook(new FileOutputStream(f));
		String nowDate = DataUtils.date2Str(new Date(),
				DataUtils.yyyymmddhhmmss);
		WritableSheet ws = wwb.createSheet(nowDate, 0);
		// 给sheet电子版中所有的列设置默认的列的宽度;
		ws.getSettings().setDefaultColumnWidth(30);
		// // 设置字体样式
		// WritableFont wfc = new WritableFont(WritableFont.ARIAL,
		// 13,WritableFont.NO_BOLD, false);
		// WritableCellFormat wcfFC = new WritableCellFormat(wfc);
		// 遍历内容
		for (int i = 0; i < mapList.size(); i++) {
			Map<String, String> map = mapList.get(i);
			for (Entry<String, String> entry : map.entrySet()) {
				String boxBarCode = (String) entry.getKey();
				String measureBarCode = entry.getValue();
				Label labelBoxBarCode = new jxl.write.Label(i, 0,
						boxBarCode);
				ws.addCell(labelBoxBarCode);
				Label nullLabel = new jxl.write.Label(i, 1, "");
				ws.addCell(nullLabel);
				if (measureBarCode != null && measureBarCode.length() > 0) {
					String[] measureBarCodeArray = measureBarCode
							.split(",");
					int measureSize = measureBarCodeArray.length;
					for (int j = 0; j < measureSize; j++) {
						String measureBarCodeValue = measureBarCodeArray[j];
						Label measureLabel = new jxl.write.Label(i, j + 2,
								measureBarCodeValue);
						ws.addCell(measureLabel);
					}
				}
			}
		}
		// 写入Exel工作表
		wwb.write();
		// 关闭Excel工作薄对象
		wwb.close();
		
	}
	/**
	 * 导出的公共类
	 * 
	 * @param list
	 *            头与内容存放在String[]数组里 一起封装在list里
	 * @throws Exception
	 */
	public static void writeExcelRow(List<Map<String, String>> mapList,
			String defaultFileName) throws Exception {
		// 弹出保存框
		JFileChooser file = ExcelUtil.getFile(defaultFileName);
		// 判断是否关闭或取消保存框
		if (file != null) {
			// 的到保存路径
			String fpath = file.getSelectedFile().getAbsolutePath() + ".xls";
			// 创建文件
			File f = new File(fpath);
			WritableWorkbook wwb = Workbook
					.createWorkbook(new FileOutputStream(f));
			String nowDate = DataUtils.date2Str(new Date(),
					DataUtils.yyyymmddhhmmss);
			WritableSheet ws = wwb.createSheet(nowDate, 0);
			// 给sheet电子版中所有的列设置默认的列的宽度;
			ws.getSettings().setDefaultColumnWidth(30);
			// // 设置字体样式
			// WritableFont wfc = new WritableFont(WritableFont.ARIAL,
			// 13,WritableFont.NO_BOLD, false);
			// WritableCellFormat wcfFC = new WritableCellFormat(wfc);
			// 遍历内容
			for (int i = 0; i < mapList.size(); i++) {
				Map<String, String> map = mapList.get(i);
				for (Entry<String, String> entry : map.entrySet()) {
					String boxBarCode = (String) entry.getKey();
					String measureBarCode = entry.getValue();
					Label labelBoxBarCode = new jxl.write.Label(0, i,
							boxBarCode);
					ws.addCell(labelBoxBarCode);
					Label nullLabel = new jxl.write.Label(1, i, "");
					ws.addCell(nullLabel);
					if (measureBarCode != null && measureBarCode.length() > 0) {
						String[] measureBarCodeArray = measureBarCode
								.split(",");
						int measureSize = measureBarCodeArray.length;
						for (int j = 0; j < measureSize; j++) {
							String measureBarCodeValue = measureBarCodeArray[j];
							Label measureLabel = new jxl.write.Label(j+2,i,
									measureBarCodeValue);
							ws.addCell(measureLabel);
						}
					}
				}
			}
			// 写入Exel工作表
			wwb.write();
			// 关闭Excel工作薄对象
			wwb.close();
			// 消息框
			JOptionPane.showMessageDialog(null, "文件保存到:" + fpath, "保存成功",	JOptionPane.INFORMATION_MESSAGE);
		}
	}
	/**
	 * 导出的公共类
	 * 
	 * @param list
	 *            头与内容存放在String[]数组里 一起封装在list里
	 * @throws Exception
	 */
	public static void writeExcel(List<Map<String, String>> mapList,
			String defaultFileName) throws Exception {
		// 弹出保存框
		JFileChooser file = ExcelUtil.getFile(defaultFileName);
		// 判断是否关闭或取消保存框
		if (file != null) {
			// 的到保存路径
			String fpath = file.getSelectedFile().getAbsolutePath() + ".xls";
			// 创建文件
			File f = new File(fpath);
			WritableWorkbook wwb = Workbook
					.createWorkbook(new FileOutputStream(f));
			String nowDate = DataUtils.date2Str(new Date(),
					DataUtils.yyyymmddhhmmss);
			WritableSheet ws = wwb.createSheet(nowDate, 0);
			// 给sheet电子版中所有的列设置默认的列的宽度;
			ws.getSettings().setDefaultColumnWidth(30);
			// // 设置字体样式
			// WritableFont wfc = new WritableFont(WritableFont.ARIAL,
			// 13,WritableFont.NO_BOLD, false);
			// WritableCellFormat wcfFC = new WritableCellFormat(wfc);
			// 遍历内容
			for (int i = 0; i < mapList.size(); i++) {
				Map<String, String> map = mapList.get(i);
				for (Entry<String, String> entry : map.entrySet()) {
					String boxBarCode = (String) entry.getKey();
					String measureBarCode = entry.getValue();
					Label labelBoxBarCode = new jxl.write.Label(i, 0,
							boxBarCode);
					ws.addCell(labelBoxBarCode);
					Label nullLabel = new jxl.write.Label(i, 1, "");
					ws.addCell(nullLabel);
					if (measureBarCode != null && measureBarCode.length() > 0) {
						String[] measureBarCodeArray = measureBarCode
								.split(",");
						int measureSize = measureBarCodeArray.length;
						for (int j = 0; j < measureSize; j++) {
							String measureBarCodeValue = measureBarCodeArray[j];
							Label measureLabel = new jxl.write.Label(i, j + 2,
									measureBarCodeValue);
							ws.addCell(measureLabel);
						}
					}
				}
			}
			// 写入Exel工作表
			wwb.write();
			// 关闭Excel工作薄对象
			wwb.close();
			// 消息框
			JOptionPane.showMessageDialog(null, "文件保存到:" + fpath, "保存成功",	JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public static JFileChooser getFile(String defaultFileName) {
		String savePath = FileUtils.readDate();
		// 默认打开D盘
		JFileChooser file = new MyChooser(savePath+":/");
		// 下面这句是去掉显示所有文件这个过滤器。
		file.setAcceptAllFileFilterUsed(false);
		// 添加excel文件的过滤器
		file.addChoosableFileFilter(new ExcelFileFilter("xls"));
		String nowDate = DataUtils.date2Str(new Date(), DataUtils.yyyyMMdd);
		file.setSelectedFile(new File(defaultFileName));
		int result = file.showSaveDialog(null);
		// JFileChooser.APPROVE_OPTION是个整型常量，代表0。就是说当返回0的值我们才执行相关操作，否则什么也不做。
		if (result == JFileChooser.APPROVE_OPTION) {

			// 获得你选择的文件绝对路径。并输出。当然，我们获得这个路径后还可以做很多的事。
			String path = file.getSelectedFile().getAbsolutePath();
			System.out.println(path);
		} else {
			file = null;
			System.out.println("你已取消并关闭了窗口！");

		}
		return file;
	}

	// 文件过滤器 只保存xls文件
	private static class ExcelFileFilter extends FileFilter {
		String ext;

		ExcelFileFilter(String ext) {
			this.ext = ext;
		}

		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			String fileName = f.getName();
			int index = fileName.lastIndexOf('.');

			if (index > 0 && index < fileName.length() - 1) {
				String extension = fileName.substring(index + 1).toLowerCase();
				if (extension.equals(ext))
					return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			if (ext.equals("xls")) {
				return "Microsoft Excel文件(*.xls)";
			}
			return "";
		}

	}

	private static class MyChooser extends JFileChooser {
		/**
   * 
   */
		private static final long serialVersionUID = 1L;

		MyChooser(String path) {
			super(path);
		}

		public void approveSelection() {
			File file = this.getSelectedFile();
			if (file.exists()) {
				int copy = JOptionPane.showConfirmDialog(null, "是否要覆盖当前文件？",
						"保存", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (copy == JOptionPane.YES_OPTION)
					super.approveSelection();
			} else
				super.approveSelection();
		}

	}
}