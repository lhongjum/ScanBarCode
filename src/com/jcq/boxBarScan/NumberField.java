package com.jcq.boxBarScan;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
/**
 * @ClassName: MainFrame
 * @Description:�����ı���.
 * @author jcq
 * @date 2015-3-24 ����08:38:48
 */
public class NumberField extends JTextField {
 
     public NumberField(int cols) {
         super(cols);
     }
 
     public NumberField() {
	 }

	protected Document createDefaultModel() {
         return new NumberDocument();
     }
 
     static class NumberDocument extends PlainDocument {
 
         public void insertString(int offs, String str, AttributeSet a) 
             throws BadLocationException {
 
             if (str == null) {
                 return;
             }
             char[] upper = str.toCharArray();
             char[] temp = new char[upper.length];
             for (int i = 0; i < upper.length; i++) {
                if(upper[i] >= '0' && upper[i] <= '9') {
                	temp[i] = upper[i];
                }
             }
             super.insertString(offs, new String(temp).trim(), a);
         }
     }
 }
