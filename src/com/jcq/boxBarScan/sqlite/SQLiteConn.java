package com.jcq.boxBarScan.sqlite;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
/**
 * @ClassName: SQLiteConn
 * @Description: 数据库连接.
 * @author jcq
 * @date 2015-3-24 上午08:38:48
 */
public class SQLiteConn implements Serializable {
	private static final long serialVersionUID = 102400L;
	String dbFile = "barCodeDb.db";
	/**
	 * 与SQLite嵌入式数据库建立连接
	 * @return Connection
	 * @throws Exception
	 */
	public Connection getConnection() throws Exception {
		Connection connection = null ;
		try{
			Class.forName("org.sqlite.JDBC", true, this.getClass().getClassLoader()) ;
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
		}catch (Exception e) {
			throw new Exception("" + e.getLocalizedMessage(), new Throwable("可能由于数据库文件受到非法修改或删除。")) ;
		}
		return connection ;
	}
}
