package com.tld.voice.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.*;
import javax.xml.xpath.*;
/**
 * @ClassName: DBUtils
 * @Description:excel数据操作工具.
 * @author jcq
 * @date 2015-3-24 上午08:38:48
 */
public class DBUtils {
    public static void main(String[] args) {
    	getBarCodeList();
//    	addBarCodeRecord("21", null);
//    	System.out.println(isBarCodeExists("191110012445"));
//    	if(isBarCodeExists("191110012445")){
//    		delBarCodeRecord("191110012445");
//    	}
//        DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
//        Element theBook=null, theElem=null, root=null;
//        try {
//            factory.setIgnoringElementContentWhitespace(true);
//           
//            DocumentBuilder db=factory.newDocumentBuilder();
//            Document xmldoc=db.parse(new File("DB.xml"));
//            root=xmldoc.getDocumentElement();
//           
//            //--- 新建一本书开始 ----
//            theBook=xmldoc.createElement("RECORD");
//            theElem=xmldoc.createElement("BOX_BAR_CODE");
//            theElem.setTextContent("191110012445");
//            theBook.appendChild(theElem);
//           
//            theElem=xmldoc.createElement("MEASURE_BAR_CODE");
//            theElem.setTextContent("35401101215445566445");
//            theBook.appendChild(theElem);
//
//            root.appendChild(theBook);
//            theBook=(Element) selectSingleNode("/DB/RECORD[BOX_BAR_CODE='191110012445']", root);
//            if(theBook!=null){
//            	 System.out.println(theBook.getElementsByTagName("BOX_BAR_CODE").item(0).getTextContent());
//            }else{
//            	System.out.println("空 ");
//            }
           
            
//            output(xmldoc);
//            //--- 新建一本书完成 ----
//
//            //--- 下面对《哈里波特》做一些修改。 ----
//            //--- 查询找《哈里波特》----
//            theBook=(Element) selectSingleNode("/books/book[name='哈里波特']", root);
//            System.out.println("--- 查询找《哈里波特》 ----");
//            output(theBook);
//            //--- 此时修改这本书的价格 -----
//            theBook.getElementsByTagName("price").item(0).setTextContent("15");//getElementsByTagName返回的是NodeList，所以要跟上item(0)。另外，getElementsByTagName("price")相当于xpath的".//price"。
//            System.out.println("--- 此时修改这本书的价格 ----");
//            output(theBook);
//            //--- 另外还想加一个属性id，值为B01 ----
//            theBook.setAttribute("id", "B01");
//            System.out.println("--- 另外还想加一个属性id，值为B01 ----");
//            output(theBook);
//            //--- 对《哈里波特》修改完成。 ----
//
//            //--- 要用id属性删除《三国演义》这本书 ----
//            theBook=(Element) selectSingleNode("/books/book[@id='B02']", root);
//            System.out.println("--- 要用id属性删除《三国演义》这本书 ----");
//            output(theBook);
//            theBook.getParentNode().removeChild(theBook);
//            System.out.println("--- 删除后的ＸＭＬ ----");
//            output(xmldoc);
//
//            //--- 再将所有价格低于10的书删除 ----
//            NodeList someBooks=selectNodes("/books/book[price<10]", root);
//            System.out.println("--- 再将所有价格低于10的书删除 ---");
//            System.out.println("--- 符合条件的书有　"+someBooks.getLength()+"本。 ---");
//            for(int i=0;i<someBooks.getLength();i++) {
//                someBooks.item(i).getParentNode().removeChild(someBooks.item(i));
//            }
//            output(xmldoc);

//            saveXml("DB.xml", xmldoc);
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        } catch (SAXException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
    public static void addBarCodeRecord(String boxBarCode,String measureBarCode){
      DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
      Element theBook=null, theElem=null, root=null;
      try {
          factory.setIgnoringElementContentWhitespace(true);
          DocumentBuilder db=factory.newDocumentBuilder();
          Document xmldoc=db.parse(new File("DB.xml"));
          root=xmldoc.getDocumentElement();
          //--- 新建一本书开始 ----
          theBook=xmldoc.createElement("RECORD");
          theElem=xmldoc.createElement("BOX_BAR_CODE");
          theElem.setTextContent(boxBarCode);
          theBook.appendChild(theElem);
         
          theElem=xmldoc.createElement("MEASURE_BAR_CODE");
          theElem.setTextContent(measureBarCode);
          theBook.appendChild(theElem);

          root.appendChild(theBook);
          saveXml("DB.xml", xmldoc);
      } catch (ParserConfigurationException e) {
          e.printStackTrace();
      } catch (SAXException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }
    }
    //删除条码对应的记录.
    public static void delBarCodeRecord(String barCode){
    	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        Element  root=null;
        try {
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db=factory.newDocumentBuilder();
            Document xmldoc=db.parse(new File("DB.xml"));
            root=xmldoc.getDocumentElement();
            NodeList boxData=selectNodes("/DB/RECORD[BOX_BAR_CODE='"+barCode+"']", root);
            for(int i=0;i<boxData.getLength();i++) {
            	boxData.item(i).getParentNode().removeChild(boxData.item(i));
            }
            NodeList measureData=selectNodes("/DB/RECORD[MEASURE_BAR_CODE='"+barCode+"']", root);
            for(int i=0;i<measureData.getLength();i++) {
            	measureData.item(i).getParentNode().removeChild(measureData.item(i));
            }
            saveXml("DB.xml", xmldoc);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //判断条码是否存在
    public static boolean isBarCodeExists(String barCode){
    	boolean flag = false;
    	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        Element theBook=null, theElem=null, root=null;
        try {
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db=factory.newDocumentBuilder();
            Document xmldoc=db.parse(new File("DB.xml"));
            root=xmldoc.getDocumentElement();
            theBook=(Element) selectSingleNode("/DB/RECORD[BOX_BAR_CODE='"+barCode+"']", root);
            if(theBook!=null){
            	return true;
            }else{
            	theBook=(Element) selectSingleNode("/DB/RECORD[MEASURE_BAR_CODE='"+barCode+"']", root);
            	if(theBook!=null){
            		return true;
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }
    //查询计量器具条形码是否存在.
    public static boolean isMeasureBarCodeExists(String measureBarCode){
    	boolean flag = false;
    	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        Element theBook=null, theElem=null, root=null;
        try {
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db=factory.newDocumentBuilder();
            Document xmldoc=db.parse(new File("DB.xml"));
            root=xmldoc.getDocumentElement();
            theBook=(Element) selectSingleNode("/DB/RECORD[MEASURE_BAR_CODE='"+measureBarCode+"']", root);
            if(theBook!=null){
            	flag = true;
            } 
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }
    //查询周转箱条形码是否存在.
    public static boolean isBoxBarCodeExists(String boxBarCode){
    	boolean flag = false;
    	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        Element theBook=null, theElem=null, root=null;
        try {
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db=factory.newDocumentBuilder();
            Document xmldoc=db.parse(new File("DB.xml"));
            root=xmldoc.getDocumentElement();
            theBook=(Element) selectSingleNode("/DB/RECORD[BOX_BAR_CODE='"+boxBarCode+"']", root);
            if(theBook!=null){
            	flag = true;
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public static void output(Node node) {//将node的XML字符串输出到控制台
        TransformerFactory transFactory=TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("encoding", "UTF-8");
            transformer.setOutputProperty("indent", "yes");

            DOMSource source=new DOMSource();
            source.setNode(node);
            StreamResult result=new StreamResult();
            result.setOutputStream(System.out);
           
            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }  
    }
    //查询数量.
    public static int getNodeCnt() { 
    	int cnt = 0;
    	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        Element  root=null;
        try {
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db=factory.newDocumentBuilder();
            Document xmldoc=db.parse(new File("DB.xml"));
            root = xmldoc.getDocumentElement();
            NodeList contents = root.getChildNodes();
            int contentNum = 0;
            for (int i = 0; i < contents.getLength(); i++) {
	            org.w3c.dom.Node content = contents.item(i);
	            if(content.getNodeName().equals("RECORD")){
	            	contentNum++;
	            }
            } 
            return contentNum;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cnt;
    }
    //便利xml数据..
    public static void getBarCodeList() { 
    	Set<String> measureResult = new HashSet<String>();
    	Set<String> boxResult = new HashSet<String>();
    	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        Element  root=null;
        try {
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db=factory.newDocumentBuilder();
            Document xmldoc=db.parse(new File("DB.xml"));
            root = xmldoc.getDocumentElement();
            NodeList contents = root.getChildNodes();
            for (int i = 0; i < contents.getLength(); i++) {
	            org.w3c.dom.Node content = contents.item(i);
	            if(content.getNodeName().equals("RECORD")){
	            	System.out.println(content.getTextContent());
	            	if(content.getNodeName().equals("BOX_BAR_CODE")){
		            	if(content.getTextContent()!=null&&content.getTextContent().length()>0){
		            		boxResult.add(content.getTextContent());
		            	}
		            }
		            if(content.getNodeName().equals("MEASURE_BAR_CODE")){
		            	if(content.getTextContent()!=null&&content.getTextContent().length()>0){
		            		measureResult.add(content.getTextContent());
		            	}
		            }
	            }
	            
            } 
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static Node selectSingleNode(String express, Object source) {//查找节点，并返回第一个符合条件节点
        Node result=null;
        XPathFactory xpathFactory=XPathFactory.newInstance();
        XPath xpath=xpathFactory.newXPath();
        try {
            result=(Node) xpath.evaluate(express, source, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
       
        return result;
    }
   
    public static NodeList selectNodes(String express, Object source) {//查找节点，返回符合条件的节点集。
        NodeList result=null;
        XPathFactory xpathFactory=XPathFactory.newInstance();
        XPath xpath=xpathFactory.newXPath();
        try {
            result=(NodeList) xpath.evaluate(express, source, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
       
        return result;
    }
   
    public static void saveXml(String fileName, Document doc) {//将Document输出到文件
        TransformerFactory transFactory=TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("indent", "yes");

            DOMSource source=new DOMSource();
            source.setNode(doc);
            StreamResult result=new StreamResult();
            result.setOutputStream(new FileOutputStream(fileName));
           
            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }  
    }
}